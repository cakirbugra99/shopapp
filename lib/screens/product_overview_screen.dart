import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/cart.dart';
import 'package:shop_app/screens/cart_screen.dart';
import 'package:shop_app/widgets/app_drawer.dart';
import 'package:shop_app/widgets/badge.dart';
import 'package:shop_app/widgets/products_grid.dart';

import '../providers/Entities/cart_item.dart';

enum FilterOptions { Favorites, All }

class ProductOvervievScreen extends StatefulWidget {
  //access productProvider, listener of poroducts list

  @override
  State<ProductOvervievScreen> createState() => _ProductOvervievScreenState();
}

class _ProductOvervievScreenState extends State<ProductOvervievScreen> {
  var _showOnlyFavorite = false;
  @override
  Widget build(BuildContext context) {
    //no need to reach a data with provider because of local state
    // final productsContainer = Provider.of<Products>(context)
    return Scaffold(
      appBar: AppBar(
        title: Text('MyShop'),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (FilterOptions selectedValue) {
              setState(() {
                if (selectedValue == FilterOptions.Favorites) {
                  _showOnlyFavorite = true;
                } else {
                  _showOnlyFavorite = false;
                }
              });
            },
            icon: Icon(
              Icons.filter_list_rounded,
            ),
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text('Only Favorites'),
                value: FilterOptions.Favorites,
              ),
              PopupMenuItem(
                child: Text('Show All'),
                value: FilterOptions.All,
              ),
            ],
          ),
          Consumer<Cart>(
            builder: (_, cartData, ch) => Badge(
              value: cartData.itemCount.toString(),
              child: ch!, //will not be null
            ),

            //it's outside the badge becouse we don't
            // need to render it again
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
              ),
              onPressed: () {
                //when press route to Cart Screen
                Navigator.of(context).pushNamed(CartScreen.routeName);
              },
            ),
          )
        ],
      ),
      drawer: AppDrawer(),
      body: ProductsGrid(_showOnlyFavorite),
    );
  }
}

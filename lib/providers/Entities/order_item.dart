import 'package:shop_app/providers/Entities/cart_item.dart';

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> orderProducts;
  final DateTime dateTime;

  OrderItem(
      {required this.id,
      required this.amount,
      required this.orderProducts,
      required this.dateTime});
}
